import dtermbox;

int main(string[] args)
{
	auto termBox = new TermBox();

	Cell cell = {'@', Color.green, Color.def};
	Cell floor = {'.', Color.red, Color.def};

	int width = termBox.width;
	int height = termBox.height;

	termBox.cls();

	// Yes I know there's a better way to do this, I just cannot remember it
	// atm :S
	for (int y = 0; y < 10; y++)
	{
		for (int x = 0; x < 10; x++)
		{
			if (y != 0 && y != 9 && x != 0 && x != 9)
				termBox.putcell(floor, x, y);
			else
				termBox.putcell(cell, x, y);
		}
	}

	termBox.update();

	termBox.process();

	delete termBox;

	import std.stdio: writefln;
	writefln("Terminal width: %s\nTerminal height: %s", width, height);

	return 0;
}

