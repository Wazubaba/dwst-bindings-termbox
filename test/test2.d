import dtermbox;

int main(string[] args)
{
	auto termBox = new TermBox();

	int width = termBox.width;
	int height = termBox.height;

	delete termBox;

	import std.stdio: writefln;
	writefln("Terminal width: %s\nTerminal height: %s", width, height);

	return 0;
}

