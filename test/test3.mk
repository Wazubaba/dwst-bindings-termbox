BIN=test3

$(BIN):
	$(DC) $(DFLAGS) $(LIB) -I../src $(BIN).d -of$@

clean:
	-$(RM) $(BIN)
