export DC= ldc2

SRC= $(shell find . -type f ! -name "*skip.*" -and ! -path "*skip.*" -name "*.d")
OBJ= $(SRC:%.d=%.o)
BIN= $(SRC:%.d=%)
MKS= $(SRC:%.d=%.mk)

export LIB=../libdtermbox.a

all: $(OBJ)

%.o: %.mk
	$(MAKE) -f $<

clean:
	-$(RM) *.o $(BIN)
