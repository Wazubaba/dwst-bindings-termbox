module dtermbox;

/**
	DTermbox - D-ified wrapper
	==========================
	This module serves as a wrapper around the wrapper. It basically makes
	termbox more D-like.

	TODO: This should _not_ depend on the 1:1 wrapper at all. It ought to be
	two entirely seperate bindings.
**/

// Minimal wrapper for function prototypes
extern (C)
{
	int tb_init();
	int tb_init_file(const char* name);
	int tb_init_fd(int descriptor);
	void tb_shutdown();
	int tb_width();
	int tb_height();
	void tb_clear();
	void tb_set_clear_attributes(wchar fg, wchar bg);
	void tb_present();
	void tb_set_cursor(int cx, int cy);
	void tb_put_cell(int x, int y, const Cell* cell);
	void tb_change_cell(int x, int y, char ch, wchar fg, wchar bg);
	void tb_blit(int x, int y, int w, int h, const Cell[] cells);
	Cell* tb_cell_buffer();
	int tb_select_input_mode(int mode);
	int tb_select_output_mode(int mode);
	int tb_peek_event(Event *event, int timeout);
	int tb_poll_event(Event *event);
	int tb_utf8_char_length(char c);
	int tb_utf8_char_to_unicode(dchar *val, const char c);
	int tb_utf8_unicode_to_char(char *val, dchar c);
}

import std.stdio: writeln;
import core.time: Duration;
import std.conv: to;

enum Key
{
	F1 = 0xFFFF-0,
	F2 = 0xFFFF-1,
	F3 = 0xFFFF-2,
	F4 = 0xFFFF-3,
	F5 = 0xFFFF-4,
	F6 = 0xFFFF-5,
	F7 = 0xFFFF-6,
	F8 = 0xFFFF-7,
	F9 = 0xFFFF-8,
	F10 = 0xFFFF-9,
	F11 = 0xFFFF-10,
	F12 = 0xFFFF-11,

	INSERT = 0xFFFF-12,
	DELETE = 0xFFFF-13,
	HOME = 0xFFFF-14,
	END = 0xFFFF-15,
	PGUP = 0xFFFF-16,
	PGDN = 0xFFFF-17,
	ARROW_UP = 0xFFFF-18,
	ARROW_DOWN = 0xFFFF-19,
	ARROW_LEFT = 0xFFFF-20,
	ARROW_RIGHT = 0xFFFF-21,
	MOUSE_LEFT = 0xFFFF-22,
	MOUSE_RIGHT = 0xFFFF-23,
	MOUSE_MIDDLE = 0xFFFF-24,
	MOUSE_RELEASE = 0xFFFF-25,
	MOUSE_WHEEL_UP = 0xFFFF-26,
	MOUSE_WHEEL_DOWN = 0xFFFF-27,

	// Some of these clash, but that's how they are in termbox.h so idk *shrugs*
	CTRL_TILDE = 0x00,
	CTRL_2 = 0x00,
	CTRL_A = 0x01,
	CTRL_B = 0x02,
	CTRL_C = 0x03,
	CTRL_D = 0x04,
	CTRL_E = 0x05,
	CTRL_F = 0x06,
	CTRL_G = 0x07,
	BACKSPACE = 0x08,
	CTRL_H = 0x08,
	TAB = 0x09,
	CTRL_I = 0x09,
	CTRL_J = 0x0A,
	CTRL_K = 0x0B,
	CTRL_L = 0x0C,
	ENTER = 0x0D,
	CTRL_M = 0x0D,
	CTRL_N = 0x0E,
	CTRL_O = 0x0F,
	CTRL_P = 0x10,
	CTRL_Q = 0x11,
	CTRL_R = 0x12,
	CTRL_S = 0x13,
	CTRL_T = 0x14,
	CTRL_U = 0x15,
	CTRL_V = 0x16,
	CTRL_W = 0x17,
	CTRL_X = 0x18,
	CTRL_Y = 0x19,
	CTRL_Z = 0x1A,
	ESC = 0x1B,
	CTRL_LSQ_BRACKET = 0x1B,
	CTRL_3 = 0x1B,
	CTRL_4 = 0x1C,
	CTRL_BACKSLASH = 0x1C,
	CTRL_5 = 0x1D,
	CTRL_RSQ_BRACKET = 0x1D,
	CTRL_6 = 0x1E,
	CTRL_7 = 0x1F,
	CTRL_SLASH = 0x1F,
	CTRL_UNDERSCORE = 0x1F,
	SPACE = 0x20,
	BACKSPACE2 = 0x7F,
	CTRL_8 = 0x7F
}

enum Attribute
{
	bold = 0x0100,
	underline = 0x0200,
	reverse = 0x0400
}

enum EventType
{
	error = -1,
	noEvent = 0,
	key = 1,
	resize = 2,
	mouse = 3
}

enum InputMode
{
	/// Return the current InputMode.
	current = 0,
	/// Unknown escape sequences will resolve as tbInput.ESC. Default mode.
	escape = 1,
	/// Unknown escape sequences will resolve as tbMod.ALT
	alt = 2,
	/// Can be applied bit-wise to either escape or alt mode, defaults to escape.
	mouse = 4
}

enum OutputMode
{
	/// Return the current OutputMode.
	current = 0,
	/// Provides the standard 8 colors, which are found in the CellColors enum. Default mode.
	normal = 1,
	/// Switches to 256 color suport mode.
	/// 0x00 - 0x07: Standard 8 colors
	/// 0x08 - 0x0f: Same standard 8 colors, but with the bold attribute
	/// 0x10 - 0xe7: 216 seperate colors
	/// 0xe8 - 0xff: 24 shades of grey
	color256 = 2,

	/// Helper for 256 mode that bypasses the need for offsets to access 3rd range
	color216 = 4,
	/// Helper for 256 mode that bypasses the need for offsets to access 4th range
	grayscale = 8
}

enum Color
{
	def = 0x00,
	black = 0x01,
	red = 0x02,
	green = 0x03,
	yellow = 0x04,
	blue = 0x05,
	magenta = 0x06,
	cyan = 0x07,
	white = 0x08
}

/// Mixin that should be added to the main interface control function.
string setupTermbox()
{
	string rhs = "startTB();\nscope (exit) stopTB();";
	debug rhs ~= "\npragma(msg, \"Termbox initialised!\");";

	return rhs;
}

struct Event
{
	char type;
	char mod; /* modifiers to either 'key' or 'ch' below */
	wchar key; /* one of the TB_KEY_* constants */
	dchar ch; /* unicode character */
	dchar w;
	dchar h;
	dchar x;
	dchar y;
}

//alias EventType = tbEvent;

/++
	The preferred method of working with termbox tbh. Support for doing
	it without a class is available, just this keeps things nice and
	organized IMHO.
++/
struct Cell
{
	dchar ch;
	wchar fg;
	wchar bg;
}

class TermBox
{
	// Aliases to make things fit in a bit better
	//alias putch  = tb_put_cell;
	alias setAttrb = tb_set_clear_attributes;

	// I still am not certain if @property is worth bothering, same effect as
	// without the keyword...
	@property int width() { return tb_width(); }
	@property int height() { return tb_height(); }

	/// Last event returned from processing
	Event lastEvent;

	/// Last result returned from processing
	int lastEventResult;

	/++
		Clear the screen to the default color settings.
	++/
		alias cls = tb_clear;

	/++
		Update the screen buffer with all newly altered data.
	++/
		alias update = tb_present;

	/++
		Draw a Cell to the screen at a given coordinate.
		Note:
			This function does _not_ test if the given coordinates are within
			the screen-space.
	++/
	void putcell(Cell cell, int x, int y) { tb_put_cell(x, y, &cell); }

	/++
		Set the default background and foreground colors.
		Arguments:
			wchar fg - Foreground default color
			wchar bg - Background default color
	++/
	alias setDefaultColor = tb_set_clear_attributes;

	/++
		Set cursor position.
		Arguments:
			int x - Desired X position of the cursor
			int y - Desired Y position of the cursor
	++/
	alias setCursorPosition = tb_set_cursor;

	/++
		Draw a char to a specific position.
		Arguments:
			int x - Desired X position for the new char
			int y - Desired y position for the new char
			char ch - Desired char to draw
			wchar fg - Foreground color of the new char
			wchar bg - Background color of the new char
	++/
	alias putchar = tb_change_cell;

	/++
		Alter how input is handled.
		Arguments:
			int mode - InputMode to use for input handling
		Returns:
			Non-zero mode if specified mode is InputMode.current
	++/
	int inputMode(int mode) { return tb_select_input_mode(mode); }

	/++
		Alter how output is handled.
		Arguments:
			int mode - OutputMode to use for output handling
		Returns:
			Non-zero mode if specified mode is OutputMode.current
	++/
	int outputMode(int mode) { return tb_select_output_mode(mode); }

	/// Wait for an event indefinitely
	int process()
	{
		this.lastEventResult = tb_poll_event(&lastEvent);
		return this.lastEventResult;
	}

	/++
		Wait for an event up untill a specified Duration.
		Arguments:
			Duration timelimit - How long to wait for an event
		Returns:
			int event result
	++/
	int processUntillTimeout(Duration timelimit)
	{
		this.lastEventResult = tb_peek_event(&lastEvent, to!int(timelimit.total!"msecs"));
		return this.lastEventResult;
	}

	this() { tb_init(); }
	~this() { tb_shutdown(); }
}

alias startTB = tb_init;
alias stopTB = tb_shutdown;
