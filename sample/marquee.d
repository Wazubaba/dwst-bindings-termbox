import dtermbox;
import core.time: msecs;
import std.stdio: writeln;

void write(ref TermBox tb, string msg, int x, int y)
{
	int dx = x;
	foreach (ch; msg)
		tb.putchar(dx++, y, ch, CellColors.def, CellColors.def);
}

string marqueeText(string msg, size_t offset)
{
	string buffer;
	buffer ~= msg[offset..$];
	buffer ~= msg[0..offset];
	return buffer;
}

int main(string[] args)
{
	if (args.length < 2)
	{
		writeln("Please provide a few arguments for the marquee message!");
		return -2;
	}

	string msg;
	foreach (arg; args[1..$])
		msg ~= " " ~ arg;
	msg = msg[1..$];

	auto termbox = new TermBox();

	if (termbox.height < 2)
	{
		delete termbox;
		writeln("Display too small for marquee, need at least two rows!");
		return -1;
	}

	int offset = 0;

	termbox.write("Press <escape> to exit!", 0, termbox.height - 2);
	termbox.write(msg, 0, termbox.height - 1);

	while (termbox.processUntillTimeout(100.msecs) != -1)
	{
		termbox.update();
		switch (termbox.lastEventResult) with (EventType)
		{
			case (error):
				delete termbox;
				writeln("Error handling input!");
				return -1;
			case (key):
				// Test if escape was hit and shutdown if so
				if (termbox.lastEvent.key == Key.ESC)
					delete termbox;
					writeln("Shutting down!");
					goto OUT;
			default:
				// Animate the marquee
				if (offset == msg.length) offset = 0;
				termbox.write(marqueeText(msg, offset++), 0, termbox.height-1);
				break;
		}
	}
	delete termbox;
	writeln("uh. something broke...");
OUT:
	return 0;
}

