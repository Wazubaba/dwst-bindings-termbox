DC= ldc2

SRC= src/dtermbox.d
OBJ= $(SRC:src/%.d=.bld/%.o)

BIN= libdtermbox-binding.a
FINAL= libdtermbox.a

ifdef debug
	DFLAGS+= -d-debug
endif

ifdef release
	DFLAGS+= -release -Oz
endif

ifdef warnings
	DFLAGS+= -w
endif

ifdef build_32bit
	DFLAGS+= -m32
endif

ifdef build_64bit
	DFLAGS+= -m64
endif

.PHONY: all, shrink, distclean, clean

all: $(FINAL) dtermbox.di

$(FINAL): $(BIN)
	ar -M < mergelib.mri

support/lib/libtermbox.a:
	cd support && $(MAKE) -f builddeps.mk

$(BIN): support/lib/libtermbox.a $(OBJ)
	$(DC) $(DFLAGS) -static -lib $^ -of $@

.bld/%.o: src/%.d
	$(DC) $(DFLAGS) -c -Isrc $< -of$@

shrink:
	-strip --strip-unneeded $(BIN)

dtermbox.di:
	$(DC) $(DFLAGS) -H -Hf$@ src/dtermbox.d -Isrc -o- -c

distclean: clean
	-cd support && $(MAKE) -f builddeps.mk distclean

clean:
	-$(RM) $(BIN)
	-$(RM) $(FINAL)
	-$(RM) -r .bld
	-$(RM) dtermbox.di
