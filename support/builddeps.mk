
lib/libtermbox.a:
	cd termbox && ./waf configure --prefix=/. && ./waf && ./waf install --targets=termbox_static --destdir=..

distclean:
	-$(RM) -r lib include
	-cd termbox && ./waf distclean

